#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

if (length(args) == 1) {
  library(rmarkdown)
  rmarkdown::render(args[1])
} else {
  write("usage: run-rmarkdown-notebook <NOTEBOOK-FILE>", stderr())
  invokeRestart("abort")
}
