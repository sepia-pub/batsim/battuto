# Repository content
- `sched` contains schedulers compatible with Batsim 5 called External Decision Components (EDCs)
- `input` contains Batsim inputs: a SimGrid platform and a Batsim workload
- `flake.nix` contains reproducible debuggable Nix environments into which the simulations and the analysis can be run
- `run-simus.sh` is a simple script to run several Batsim instances in sequence
- `run-rmarkdown-notebook.R` is a simple script to render a [Rmarkdown notebook](https://rmarkdown.rstudio.com/)

# Prerequisites
- Nix installed: https://nixos.org/download/
- Flakes enabled in your Nix setup: https://nixos.wiki/wiki/Flakes#Other_Distros.2C_without_Home-Manager
- Enable the `batuto` binary cache if you do not want to recompile heavy deps: https://app.cachix.org/cache/battuto#pull

# Run simulations and visualize results
```sh
nix develop .#simulation --command ./run-simus.sh
```

This should populate a `simu-out` directory, where each directory is a simulation output.

# Analyze and visualize results
```sh
nix develop .#r-notebook --command Rscript run-rmarkdown-notebook.R ./simulation-output-analysis.Rmd
```

This should create/update a `simulation-output-analysis.html` file, that you can open with a web browser.

# See also
- Batsim 4 docs & getting started tutorials: https://batsim.rtfd.io
- Energy-related example using Batsim 4: https://framagit.org/batsim/energy-instance-example
- Example of an article artifact that runs a Batsim simulation campaign: https://zenodo.org/records/11208389
- Maël's Batsim exercises for Master's students: https://gitlab.irit.fr/sepia-pub/mael/RM4ES-practicals
