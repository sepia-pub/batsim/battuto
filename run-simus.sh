#!/usr/bin/env sh
set -eu

PLATFORM=$(realpath input/cluster_energy_128.xml)
WORKLOAD=$(realpath input/test_batsim_paper_workload_seed1.json)
SIMUOUT=$(realpath ./simu-out)

# clean previous runs
rm -rf ${SIMUOUT}

# run all simulations
for sched in 'rejecter' 'exec1by1' 'fcfs' 'easy'
do
  batsim -p ${PLATFORM} -w ${WORKLOAD} --mmax-workload -l ${EDC_LIBRARY_PATH}/lib${sched}.so 0 '' -e ${SIMUOUT}/${sched}/
done
