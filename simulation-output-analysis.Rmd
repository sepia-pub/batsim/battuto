---
title: "Simulation output analysis"
author: "Millian Poquet"
date: "2024-10-23"
params:
  simulation_output: "./simu-out"
output:
  rmdformats::readthedown
---

# Initial load and variable setup
```{r, echo = TRUE}
library(tidyverse)
library(viridis)
simu_output = params$simulation_output
```

# Summary of each simulation
```{r, echo = TRUE}
schedule_rejecter = read_csv(paste(simu_output, "rejecter", "schedule.csv", sep="/"), show_col_types = FALSE) %>% mutate(sched='rejecter')
schedule_exec1by1 = read_csv(paste(simu_output, "exec1by1", "schedule.csv", sep="/"), show_col_types = FALSE) %>% mutate(sched='exec1by1')
schedule_fcfs = read_csv(paste(simu_output, "fcfs", "schedule.csv", sep="/"), show_col_types = FALSE) %>% mutate(sched='fcfs')
schedule_easy = read_csv(paste(simu_output, "easy", "schedule.csv", sep="/"), show_col_types = FALSE) %>% mutate(sched='easy')

all_schedule = bind_rows(schedule_rejecter, schedule_exec1by1, schedule_fcfs, schedule_easy) %>% select(
  sched, consumed_joules, makespan, mean_waiting_time, nb_jobs_success, nb_jobs_rejected
)
knitr::kable(all_schedule)
```

# Gantt chart of jobs
```{r, echo = TRUE}
jobs_rejecter = read_csv(paste(simu_output, "rejecter", "jobs.csv", sep="/"), show_col_types = FALSE) %>% mutate(sched='rejecter')
jobs_exec1by1 = read_csv(paste(simu_output, "exec1by1", "jobs.csv", sep="/"), show_col_types = FALSE) %>% mutate(sched='exec1by1')
jobs_fcfs = read_csv(paste(simu_output, "fcfs", "jobs.csv", sep="/"), show_col_types = FALSE) %>% mutate(sched='fcfs')
jobs_easy = read_csv(paste(simu_output, "easy", "jobs.csv", sep="/"), show_col_types = FALSE) %>% mutate(sched='easy')

all_jobs = bind_rows(jobs_rejecter, jobs_exec1by1, jobs_fcfs, jobs_easy) %>% mutate(
  job_color = as.factor(job_id %% 6)
)

all_jobs_sep = all_jobs %>%
    separate_rows(allocated_resources, sep=" ") %>%
    separate(allocated_resources, into = c("psetmin", "psetmax"), fill="right") %>%
    mutate(psetmax = as.integer(psetmax), psetmin = as.integer(psetmin)) %>%
    mutate(psetmax = ifelse(is.na(psetmax), psetmin, psetmax))

p = all_jobs_sep %>%
  ggplot(aes(xmin=starting_time, xmax=finish_time,
             ymin=psetmin, ymax=psetmax+1)) +
  geom_rect(aes(fill=job_color), show.legend=FALSE) +
  facet_wrap(vars(sched), ncol=1) +
  theme_bw() +
  scale_fill_viridis(discrete=TRUE) +
  labs(x='Time', y='Resources')
p

p + facet_wrap(vars(sched), ncol=1, scales='free_x')
```

# Waiting time diff between EASY and FCFS?
```{r, echo = TRUE}
all_jobs_sep %>%
  filter(sched %in% c('easy', 'fcfs')) %>%
  filter(waiting_time > 1) %>%
  ggplot(aes(xmin=starting_time, xmax=finish_time,
             ymin=psetmin, ymax=psetmax+1)) +
  geom_rect(aes(fill=waiting_time)) +
  facet_wrap(vars(sched), ncol=1) +
  theme_bw() +
  scale_fill_viridis(discrete=FALSE) +
  labs(x='Time', y='Resources')
```
