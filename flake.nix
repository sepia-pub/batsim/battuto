{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=23.11";
    flake-utils.url = "github:numtide/flake-utils";
    nur-kapack = {
      url = "github:oar-team/nur-kapack/master";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    intervalset-flake = {
      url = "git+https://framagit.org/batsim/intervalset";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.flake-utils.follows = "flake-utils";
    };
    batprotocol-flake = {
      url = "git+https://framagit.org/batsim/batprotocol";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.flake-utils.follows = "flake-utils";
    };
    batsim-flake = {
      url = "git+https://framagit.org/batsim/batsim?ref=batprotocol";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.batprotocol.follows = "batprotocol-flake";
      inputs.intervalset.follows = "intervalset-flake";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, nur-kapack, intervalset-flake, flake-utils, batprotocol-flake, batsim-flake }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        py = pkgs.python3;
        pyPkgs = pkgs.python3Packages;
        kapack = nur-kapack.packages.${system};
        battuto-rev = if (self ? shortRev) then self.shortRev else self.dirtyShortRev;

        # debuggable versions of Batsim and its important deps are used to show how to debug your EDC
        # avoid it on real simulation campaigns, as it severely slowdowns your simulations
        batprotopkgs = batprotocol-flake.packages-debug.${system};
        intervalsetpkgs = intervalset-flake.packages-debug.${system};
        batpkgs = batsim-flake.packages-debug.${system};

        cppMesonDevBase = nur-kapack.lib.${system}.cppMesonDevBase;
        intervalset = intervalsetpkgs.intervalset;
        batprotocol-cpp = batprotopkgs.batprotocol-cpp;
      in rec {
        packages = {
          batsim = batpkgs.batsim;
          edc = (cppMesonDevBase {
            inherit (pkgs) stdenv lib meson ninja pkg-config;
            debug = true;
            werror = false;
            doCoverage = false;
          }).overrideAttrs(attrs: rec {
            pname = "battuto-edc";
            version = battuto-rev;
            src = pkgs.lib.sourceByRegex ./sched [
              "^meson\.build"
              "^.*\.?pp"
              "^.*\.h"
            ];
            buildInputs = [
              batprotocol-cpp
              intervalset
              pkgs.nlohmann_json
            ];
            passthru = rec {
              DEBUG_SRC_DIRS = intervalset.DEBUG_SRC_DIRS ++ batprotocol-cpp.DEBUG_SRC_DIRS ++ [ "${src}" ];
              GDB_DIR_ARGS = map (x: "--directory=" + x) DEBUG_SRC_DIRS;
            };
          });
        };
        devShells = rec {
          simulation = pkgs.mkShell {
            buildInputs = [
              packages.batsim
              packages.edc
            ];
            BATSIM_ROOT_PATH="${batsim-flake}";
            EDC_LIBRARY_PATH="${packages.edc}/lib";
          };
          r-notebook = pkgs.mkShell {
            buildInputs = [
              pkgs.pandoc
              pkgs.R
              pkgs.rPackages.rmarkdown
              pkgs.rPackages.rmdformats
              pkgs.rPackages.knitr
              pkgs.rPackages.svglite
              pkgs.rPackages.tidyverse
              pkgs.rPackages.viridis
            ];
          };
          simulation-debug = simulation.overrideAttrs (finalAttrs: previousAttrs: rec {
            DEBUG_SRC_DIRS = packages.batsim.DEBUG_SRC_DIRS ++ packages.edc.DEBUG_SRC_DIRS;
            GDB_DIR_ARGS = packages.batsim.GDB_DIR_ARGS ++ packages.edc.GDB_DIR_ARGS;
            buildInputs = previousAttrs.buildInputs ++ [ pkgs.gdb pkgs.cgdb ];
            shellHook = ''
              echo Found debug_info source paths. ${builtins.concatStringsSep ":" DEBUG_SRC_DIRS}
              echo Run the following command to automatically load these directories to gdb.
              echo gdb \$\{GDB_DIR_ARGS\}
            '';
          });
        };
      }
    );
}
